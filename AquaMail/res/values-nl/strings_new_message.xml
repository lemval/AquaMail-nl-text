<?xml version="1.0" encoding="utf-8"?>
<resources>

    <!-- New message activity -->

    <string name="new_message_activity_new">Nieuw</string>
    <string name="new_message_activity_draft">Concept</string>
    <string name="new_message_action_send">Verstuur</string>
    <string name="new_message_action_edit_quote">Wijzig citaat</string>
    <string name="new_message_action_as_new">Wijzig als nieuw</string>
    <string name="new_message_from_chooser_title">Verstuur van:</string>
    <string name="new_message_from_hint">Afzender</string>
    <string name="new_message_to_hint">Aan</string>
    <string name="new_message_cc_hint">CC</string>
    <string name="new_message_bcc_hint">BCC</string>
    <string name="new_message_reply_to_hint">Antwoord naar</string>
    <string name="new_message_subject_hint">Onderwerp van bericht</string>
    <string name="new_message_body_hint">Inhoud bericht</string>
    <string name="new_message_malformed_email_multi">Geef één of meerdere geldige emailadressen op</string>
    <string name="new_message_malformed_email_single">Laat leeg of geef slechts één emailadres op</string>
    <string name="new_message_no_subject">Geef het onderwerp van het bericht op</string>
    <string name="new_message_no_recepients">Geef een ontvanger op in het Aan:, CC: of BCC: veld</string>
    <string name="new_message_no_body">Het bericht bevat geen inhoud</string>
    <string name="new_message_draft_saved">Concept opgeslagen in %1$s</string>
    <string name="new_message_sending">Verzenden…</string>
    <string name="new_message_menu_add_cc_bcc">Toon of voeg CC/BCC toe</string>
    <string name="new_message_menu_send_now">Verzend</string>
    <string name="new_message_menu_save">Bewaar</string>
    <string name="new_message_menu_send_later">Verzend later</string>
    <string name="new_message_menu_reply_to">Specificeer adres voor antwoord</string>
    <string name="new_message_menu_add_attachment">Voeg toe</string>
    <string name="new_message_menu_add_attachment_camera">Maak een foto en voeg toe</string>
    <string name="new_message_menu_add_attachment_image_cropped">Voeg bijgesneden afbeelding toe</string>
    <string name="new_message_menu_resize_images">Verklein afbeelding</string>
    <string name="new_message_menu_delete">Verwijder</string>
    <string name="new_message_menu_edit_as_new">Wijzig als nieuw</string>
    <string name="new_message_menu_delivery_receipt">Vraag afleverbevestiging</string>
    <string name="new_message_delivery_receipt_prompt">Afleverbevestiging gevraagd</string>
    <string name="new_message_menu_read_receipt">Vraag leesbevestiging</string>
    <string name="new_message_read_receipt_prompt">Leesbevestiging gevraagd</string>
    <string name="new_message_menu_priority">Hoge prioriteit</string>
    <string name="new_message_priority_prompt">Hoge prioriteit</string>
    <string name="new_message_menu_insert_greeting">Voeg groet toe</string>
    <string name="new_message_menu_append_signature">Voeg onderschrift toe</string>
    <string name="new_message_menu_flipdog_spell_checker">Spellingscontrole (Flipdog Solutions, LLC)</string>
    <string name="new_message_insert_group_confirm">Voeg groep toe aan: %1$s?</string>
    <string name="new_message_attach_chooser">Open met…</string>
    <string name="new_message_attach_error_no_intent">Geen applicaties aanwezig om aanhangsel te selecteren</string>
    <string name="new_message_attach_error_invalid">Ongeldig of afwezig aanhangsel %1$s</string>
    <string name="new_message_attach_warning_too_large">Dit aanhangsel is erg groot: %1$s; het kan lang duren voordat het is verzonden</string>
    <string name="new_message_attach_error_removing">Verwijderen afwezige aanhangsel %1$s</string>
    <string name="new_message_attachment_size_none">Onbekende grootte</string>
    <string name="new_message_inline_not_image">Geen afbeelding: %1$s</string>
    <string name="new_message_exit_title">Concept wijzigen</string>
    <string name="new_message_exit_message">Stop wijzigen van dit concept?</string>
    <string name="new_message_exit_save">Bewaar en sluit af</string>
    <string name="new_message_exit_resume">Terug naar wijzigen</string>
    <string name="new_message_exit_abandon">Negeer wijzigingen</string>
    <string name="new_message_delete_draft_title">Concept</string>
    <string name="new_message_delete_draft_message">Dit concept echt verwijderen?</string>
    <string name="new_message_delete_draft_deleted">Concept verwijderd</string>
    <string name="new_message_send_error">Verzendfout: %1$s</string>
    <string name="new_message_send_no_content">Het bericht heeft geen inhoud</string>
    <string name="new_message_send_size_exceeded">Het bericht is groter (%1$dK) dan wat de server toestaat (%2$dK)</string>
    <string name="new_message_error_not_drafts_folder">Fout: dit is niet de map met concepten</string>

    <!-- Referenced messages -->

    <string name="new_message_ref_quote_header_toggle">Origineel bericht toevoegen</string>
    <string name="new_message_ref_quote_header_edit">Wijzig</string>
    <string name="new_message_ref_header_reply_gmail">Op %2$s schreef %1$s:</string>
    <string name="new_message_ref_header_reply_outlook">--- Origineel bericht ---</string>
    <string name="new_message_ref_header_forward">--- Doorgestuurd bericht ---</string>
    <string name="new_message_ref_field_from">Van</string>
    <string name="new_message_ref_field_date">Datum</string>
    <string name="new_message_ref_field_subject">Onderwerp</string>
    <string name="new_message_ref_field_to">Aan</string>
    <string name="new_message_ref_field_cc">CC</string>
    <string name="new_message_ref_field_bcc">BCC</string>
    <string name="new_message_ref_body_was_clipped">Verkorte tekst wordt getoond</string>
    <string name="new_message_pick_title">Kies aanhangsel</string>
    <string name="new_message_pick_attach">Voeg toe</string>
    <string name="new_message_pick_none">Geen</string>
    <string name="new_message_pick_all">Alles</string>
    <string name="new_message_pick_wait">Wacht tot het aanhangsel is opgehaald</string>
    <string name="new_message_no_accounts">Geen accounts voor verzenden mail</string>
    <string name="new_message_default_account">Standaardaccount wordt gebruikt</string>
    <string name="new_message_no_email">Deze contactpersoon heeft geen emailadres</string>
    <string name="new_message_add_email_title">Bewaar in Contacten</string>
    <string name="new_message_add_email_nothing">Alle aanwezige adressen bestaan al in Contacten</string>

    <!-- Hints for email types -->

    <string name="new_message_email_type_home">thuis</string>
    <string name="new_message_email_type_work">werk</string>

    <!-- New contact picker -->

    <string name="new_message_contacts_pager_system">Contacten</string>
    <string name="new_message_contacts_pager_recent">Recent</string>
    <string name="new_message_contacts_pager_groups">Groepen</string>
    <string name="new_message_contacts_search_hint">Zoek personen</string>
    <string name="new_message_contacts_selected_none">Annuleer</string>
    <string name="new_message_contacts_selected_some">Accepteer: %1$d</string>
    <string name="new_message_contacts_load_error">Laadfout</string>
    <string name="new_message_contacts_no_contacts">Geen contacten</string>
    <string name="new_message_contacts_no_groups">Geen groepen</string>
    <string name="new_message_contacts_no_matches">Geen overeenkomsten</string>
    <string name="new_message_contacts_loading">Laden…</string>
    <string name="new_message_menu_pick_contacts_internal">Ingebouwde contactkiezer</string>
    <string name="new_message_menu_pick_contacts_system">Contactkiezer van het systeem</string>
    <string name="new_message_contacts_group_my_contacts">Mijn Contacten</string>
    <string name="new_message_contacts_group_friends">Vrienden</string>
    <string name="new_message_contacts_group_family">Familie</string>
    <string name="new_message_contacts_group_coworkers">Collega\'s</string>
    <string name="new_message_contacts_searching_server">Zoeken contacten op server…</string>

    <!-- Starred contact picker -->

    <string name="starred_contact_picker_not_found">Geen contacten met ster gevonden</string>

    <!-- Debug logs -->

    <string name="new_message_attach_debug_logs">Diag: voeg logbestanden toe</string>

    <!-- Rich text editor -->

    <string name="new_message_menu_format_rich">Gebruik tekst met opmaak</string>
    <string name="new_message_menu_format_plain">Gebruik platte tekst</string>

    <!-- Send later -->

    <string name="new_message_send_later_other">Overig…</string>

</resources>
