///bin/true; exec /usr/bin/env go run "$0" "$@"

package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"
)

type strslice []string

func (s *strslice) String() string {
	return fmt.Sprintf("%s", *s)
}

func (s *strslice) Set(value string) error {
	fmt.Printf("%s\n", value)
	*s = append(*s, value)
	return nil
}

type Flags struct {
	root      string
	ignore    strslice
	output    strslice
	summary   bool
	redundant bool
	plurals   bool
}

func (s Flags) ShouldIgnore(lang ResLanguage) bool {
	for _, v := range s.ignore {
		if v == lang.GetShortName() {
			return true
		}
		if v == lang.GetLongName() {
			return true
		}
	}
	return false
}

func (s Flags) ShouldOutput(lang *ResLanguage) bool {
	if len(s.output) == 0 {
		return true
	}
	for _, v := range s.output {
		if v == lang.GetLongName() {
			return true
		}
	}
	return false
}

var flags Flags

type ResEntity interface {
	ValidateSelf()
	ValidateBase(base *ResEntity)
	IsTranslatable() bool
	GetName() string
	GetFile() string
	GetType() string
	Print()
}

func ShouldIgnore(f, n string) bool {
	if strings.HasSuffix(f, "_non_nls.xml") || f == "values_prefs.xml" || f == "values_ui.xml" {
		return true
	}

	if strings.HasPrefix(n, "mdm_") || strings.Index(n, "_mdm_") != -1 || strings.Index(n, "_tcx_") != -1 {
		return true
	}

	return false
}

type ResEntitySlice []ResEntity

func (a ResEntitySlice) Len() int {
	return len(a)
}

func (a ResEntitySlice) Less(i, j int) bool {
	l := a[i]
	r := a[j]

	lf := l.GetFile()
	rf := r.GetFile()

	if lf < rf {
		return true
	} else if lf > rf {
		return false
	} else {
		return l.GetName() < r.GetName()
	}
}

func (a ResEntitySlice) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type ResEntityOutput func(ResEntity)

func Print(l *ResLanguage, a ResEntitySlice, msg string, f ResEntityOutput) {
	if len(a) > 0 {
		fmt.Printf("\n*** %s: %s: %d\n", l.GetLongName(), msg, len(a))
		if len(a) > 500 {
			return
		}
		lastf := ""
		acopy := a
		sort.Sort(acopy)
		for _, re := range acopy {
			if lastf != re.GetFile() {
				lastf = re.GetFile()
				fmt.Printf("\n<!-- %s -->\n\n", lastf)
			}
			f(re)
		}
	}
}

type ResBase struct {
	Name         string `xml:"name,attr"`
	Translatable string `xml:"translatable,attr"`

	file string
	placeholders string
}

func (b *ResBase) ValidateSelf() {
	if ShouldIgnore(b.file, b.Name) {
		b.Translatable = "false"
	}
}

func (b *ResBase) ValidateBase(o *ResEntity) {
}

func ValidateValue(re ResEntity, v string) {
	s := v
	if strings.HasPrefix(v, "\"") && strings.HasSuffix(v, "\"") {
		//s = v[1:len(v)-1]
		return
	}
	if quotesExpr.FindStringIndex(s) != nil {
		FatalError(re, fmt.Sprintf("Unescaped quote characters: %s", v))
	}
	if badFmtExpr.FindStringIndex(s) != nil {
		FatalError(re, fmt.Sprintf("Bad formatting placeholders: %s", v))
	}
}

func GetFormattingPlaceholders(v string) string {
	var l = make([]string, 0, 10)
	m := gutFmtExpr.FindAllStringSubmatch(v, -1)
	for _, e := range m {
		l = append(l, e[0])
	}
	if len(l) == 0 {
		return ""
	}
	sort.Strings(l)
	s := fmt.Sprintf("%s", l)
	return s
}

func (b *ResBase) IsTranslatable() bool {
	return b.Translatable != "false"
}

func (b *ResBase) GetName() string {
	return b.Name
}

func (b *ResBase) GetFile() string {
	return b.file
}

type ResString struct {
	ResBase
	Value string `xml:",chardata"`
}

func (s *ResString) ValidateSelf() {
	s.ResBase.ValidateSelf()
	ValidateValue(s, s.Value)
	s.placeholders = GetFormattingPlaceholders(s.Value)
	if strings.HasPrefix(s.Value, "@string/") {
		s.Translatable = "false"
	}
}

func (s *ResString) ValidateBase(o *ResEntity) {
	switch t := (*o).(type) {
	default:
		FatalError(s, "Unexpected type")
		break;
	case *ResString:
		if t.placeholders != s.placeholders {
			FatalError(s, fmt.Sprintf("Non-matching placeholders: %s, %s", t.placeholders, s.placeholders))
		}
		break;
	}
}

func (s *ResString) GetType() string {
	return "string"
}

func (s *ResString) Print() {
	fmt.Printf("<string name=\"%s\">%s</string>\n", s.Name, s.Value)
}

type ResItem struct {
	Quant string `xml:"quantity,attr"`
	Value string `xml:",chardata"`
}

type ResPlurals struct {
	ResBase
	Items []ResItem `xml:"item"`
}

func (p *ResPlurals) ValidateSelf() {
	p.ResBase.ValidateSelf()
	if len(p.Items) == 0 {
		FatalError(p, "No items in plurals")
	} else if flags.plurals && len(p.Items) == 1 {
		fmt.Printf("Exactly one item in plurals %s\n", p.Name)
	}
}

func (p *ResPlurals) GetType() string {
	return "plurals"
}

func (p *ResPlurals) Print() {
	fmt.Printf("<plurals name=\"%s\">\n", p.Name)
	for _, i := range p.Items {
		fmt.Printf("\t<item quantity=\"%s\">%s</item>\n", i.Quant, i.Value)
	}
	fmt.Printf("</plurals>\n")
}

type ResStringArray struct {
	ResBase
	Items []ResItem `xml:"item"`
}

func (a *ResStringArray) ValidateSelf() {
	a.ResBase.ValidateSelf()
	trans := false
	for index, i := range a.Items {
		if !strings.HasPrefix(i.Value, "@string/") {
			trans = true
		}
		p := GetFormattingPlaceholders(i.Value)
		if index == 0 {
			a.placeholders = p
		} else if a.placeholders != p {
			FatalError(a, "Mismatching formatting placeholders")
		}
	}
	if !trans {
		a.Translatable = "false"
	}
}

func (a *ResStringArray) ValidateBase(o *ResEntity) {
	switch t := (*o).(type) {
	default:
		FatalError(a, "Unexpected type")
		break;
	case *ResStringArray:
		if len(t.Items) != len(a.Items) {
			FatalError(a, fmt.Sprintf("Non-matching array size: %d, %d", len(t.Items), len(a.Items)))
		}
		break;
	}
}

func (a *ResStringArray) GetType() string {
	return "string-array"
}

func (a *ResStringArray) Print() {
	fmt.Printf("<string-array name=\"%s\">\n", a.Name)
	for _, i := range a.Items {
		fmt.Printf("\t<item>%s</item>\n", i.Value)
	}
	fmt.Printf("</string-array>\n")
}

type ResValuesFile struct {
	XMLName      xml.Name         `xml:"resources"`
	Strings      []ResString      `xml:"string"`
	Plurals      []ResPlurals     `xml:"plurals"`
	StringArrays []ResStringArray `xml:"string-array"`

	flat []ResEntity
}

type ResLanguage struct {
	lang   string
	region string
	bit    int

	doesNotNeedTranslations ResEntitySlice
	missingTranslations     ResEntitySlice
}

func (l *ResLanguage) GetShortName() string {
	return l.lang
}

func (l *ResLanguage) GetLongName() string {
	if l.region == "" {
		return l.lang
	}
	return l.lang + "-r" + l.region
}

type ResMapEntity struct {
	base ResEntity
	bits int
}

func FatalError(re ResEntity, msg string) {
	fmt.Println("*** Fatal", re.GetFile(), re.GetName(), msg)
	os.Exit(1)
}

var GlobalDict = make(map[string]*ResMapEntity)

var valuesExpr = regexp.MustCompile(`^values-([a-z]{2})(?:-r([a-zA-Z]{2}))?$`)
var quotesExpr = regexp.MustCompile(`[^\\]['"]`)
var badFmtExpr = regexp.MustCompile(`(\$[0-9]+%[a-z])|(\$[0-9]+\$[a-z])|(%[0-9]+%[a-z])`)
var gutFmtExpr = regexp.MustCompile(`%(?:[0-9]+\$)?[a-z]`)

func getDirectoryListing(dirname string) []os.FileInfo {
	d, err := os.Open(dirname)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer d.Close()

	fi, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return fi
}

func scanRootDirectory(directory string) {
	fmt.Println("Processing:", directory)

	channel := make(chan ResEntity)

	go func() {
		scanValuesDirectory(channel, path.Join(directory, "values"))

		dir_values_v5 :=  path.Join(directory, "values-v5");
		if _, err := os.Stat(dir_values_v5); err == nil {
			scanValuesDirectory(channel, dir_values_v5)
		}
		channel <- nil
	}()

	for {
		re := <-channel
		if re == nil {
			break
		}
		n := re.GetName()
		GlobalDict[n] = &ResMapEntity{base: re, bits: 0}
	}

	langBit := 1
	langMap := make(map[string]*ResLanguage)

	for _, fi := range getDirectoryListing(directory) {
		if fi.IsDir() {
			name := fi.Name()
			m := valuesExpr.FindStringSubmatch(name)
			if m != nil && name != "values" && name != "values-en" && name != "values-en-rGB" {
				lang := ResLanguage{lang: "", region: "", bit: langBit}
				lang.lang = m[1]
				lang.region = ""
				if len(m) > 2 {
					lang.region = m[2]
				}
				if flags.ShouldIgnore(lang) {
					fmt.Println("Ignoring:", name)
				} else {
					go func() {
						scanValuesDirectory(channel, path.Join(directory, name))
						channel <- nil
					}()

					for {
						re := <-channel
						if re == nil {
							break
						}
						n := re.GetName()
						e := GlobalDict[n]
						if e == nil {
							FatalError(re, "not present in base")
						} else if (e.bits & lang.bit) != 0 {
							FatalError(re, "is already defined")
						} else if e.base.GetType() != re.GetType() {
							FatalError(re, "has inconsistent type")
						} else {
							if e.base.IsTranslatable() && re.IsTranslatable() {
								re.ValidateBase(&e.base)
							}
							if !e.base.IsTranslatable() && re.IsTranslatable() {
								lang.doesNotNeedTranslations = append(lang.doesNotNeedTranslations, re)
							} else {
								e.bits |= lang.bit
							}
						}
					}

					for _, e := range GlobalDict {
						if e.base.IsTranslatable() && (e.bits&langBit) == 0 {
							lang.missingTranslations = append(lang.missingTranslations, e.base)
						}
					}

					langMap[lang.GetLongName()] = &lang
					langBit = langBit << 1
				}
			}
		}
	}

	langSorted := make([]string, 0, len(langMap))
	for _, lang := range langMap {
		langSorted = append(langSorted, lang.GetLongName())
	}
	sort.Strings(langSorted)

	for _, langName := range langSorted {
		lang := langMap[langName]

		if flags.summary {
			fmt.Printf("*** %-10s", lang.GetLongName())
			if len(lang.missingTranslations) == 0 {
				fmt.Printf(" good\n")
			} else {
				fmt.Printf(" missing: %6d\n", len(lang.missingTranslations))
			}
		} else if flags.ShouldOutput(lang) {
			fmt.Printf("\n***\nLanguage: %-10s\n***\n\n", lang.GetLongName())

			if len(lang.doesNotNeedTranslations) == 0 && len(lang.missingTranslations) == 0 {
				fmt.Println("Good\n")
			} else {
				Print(lang, lang.doesNotNeedTranslations, "Does not need translations", func(re ResEntity) {
					fmt.Println(re.GetName())
				})

				Print(lang, lang.missingTranslations, "Missing transations", func(re ResEntity) {
					re.Print()
				})
			}
		}
	}
}

func scanValuesDirectory(channel chan ResEntity, directory string) {
	fmt.Println("Scanning:", directory)

	for _, fi := range getDirectoryListing(directory) {
		if fi.Mode().IsRegular() {
			nm := fi.Name()
			for _, re := range scanValuesFile(path.Join(directory, nm), nm) {
				channel <- re
			}
		}
	}
}

func scanValuesFile(file string, fname string) []ResEntity {
	//	fmt.Println("Processing:", file)

	xmlFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Error opening file:", err)
		os.Exit(1)
	}
	defer xmlFile.Close()

	xmlData, _ := ioutil.ReadAll(xmlFile)

	var f ResValuesFile
	xml.Unmarshal(xmlData, &f)

	// fmt.Println(file, "Strings:", len(f.Strings), ", Plurals:", len(f.Plurals), ", Arrays:", len(f.StringArrays))

	flat := make([]ResEntity, 0, 10)

	for _, s := range f.Strings {
		s := s
		s.file = fname
		s.ValidateSelf()
		flat = append(flat, &s)
	}

	for _, p := range f.Plurals {
		p := p
		p.file = fname
		p.ValidateSelf()
		flat = append(flat, &p)
	}

	for _, a := range f.StringArrays {
		a := a
		a.file = fname
		a.ValidateSelf()
		flat = append(flat, &a)
	}

	return flat
}

func main() {
	flag.StringVar(&flags.root, "d", "res", "Root res directory")
	flag.Var(&flags.ignore, "i", "Ignore this language code")
	flag.Var(&flags.output, "o", "Output this language code")
	flag.BoolVar(&flags.summary, "s", false, "Output a summary")
	flag.BoolVar(&flags.redundant, "r", false, "Detect redundant translations")
	flag.BoolVar(&flags.plurals, "p", false, "Detect plurals with 1 item")

	flag.Parse()

	scanRootDirectory(flags.root)
}
