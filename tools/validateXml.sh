#!/usr/bin/env bash

DIR="$( cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd )"

pushd $DIR >/dev/null

echo "[INFO] Scanning XML files..."

OUTPUT=AllLines.txt
OUTPUT_W=AllWords.txt

TMPOUT=alllines.tmp

rm $OUTPUT $OUTPUT_W $TMPOUT 2>/dev/null >/dev/null 

for file in `find . -name '*.xml' -type f`; do
  xmllint --noout $file
  cat $file | sed 's/<[^>]*>//g' | sed 's/^[ 	]*//g' \
	| sed '/^$/d' | sed 's/\\n/ /g' | sed 's/[…]//g' \
	>> $TMPOUT
  #sed -i '' 's///g' $file
  #sed -i '' 's/\&#8230;/…/g' $file
  sed -i '' -e '$a\' $file
done

cat $TMPOUT | sort -u > $OUTPUT
cat $TMPOUT | sed 's/%[0-9]$[sd]//g' | sed 's/[\\()"+%$>]//g' | sed "s=[-?;*',:./]==g" | tr ' ' '\n' | sort -u | sed '/[^a-zA-Z]/d' > $OUTPUT_W

rm $TMPOUT 2>/dev/null >/dev/null

if [ ! -x "$DIR/tools/findMissing" ]; then
  GO=$(go version 2>/dev/null) 
  if [ -z "$GO" ]; then
    echo "[WARN] Go is not installed, skipping check..."
  else 
    (cd tools && go build -o findMissing )
  fi
fi

for f in `find . -type d -name res`; do
  FOLDER=$(dirname $f)
  echo "[INFO] Validating languages in $FOLDER"
  pushd $FOLDER 1>/dev/null 2>/dev/null
  if [ -d "res/values" ]; then
    $DIR/tools/findMissing
    if [ $? -ne 0 ]; then
      echo "[ERROR] Language mismatch. See errors above..."
      exit 1
    fi
  else 
    echo " - Missing English values"
  fi
  popd 1>/dev/null 2>/dev/null
done

popd >/dev/null

echo "[INFO] XML valid. Please check $OUTPUT and $OUTPUT_W for language correctness..."
