Hello Michael,

You've done a lot of work on the Dutch translation for a mail app
called AquaMail (if you remember), and you did a great job.

Just a question now:

I'm the process of setting up a company to manage further development
of the app, there may be other people working on it.

Can I still use your translations?

Or in "legal speak":

Do you grant the permission to use your translations for AquaMail
under the new company?

Thanks,
-- Kostya Vasilyev

kmansoft@gmail.com

